﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;

class Program
{
    static void Main(string[] args)
    {
        string filePath = @"E:\labs\СТМП\lab1\data.dat";

        Mutex mutex = new Mutex();

        GenerateRandomNumbersToFile(filePath, 20, 10, 100);

        Console.WriteLine("Натисніть пробіл, щоб почати сортування...");
        while (true)
        {
            if (Console.ReadKey(true).Key == ConsoleKey.Spacebar)
            {
                Console.WriteLine("Сортування розпочато...");
                List<int> data = ReadDataFromFile(filePath, mutex);
                BubbleSortWithLogging(data, filePath, mutex);
                Console.WriteLine("Сортування завершено!");
                break;
            }
        }
        Console.ReadLine();
    }

    static void GenerateRandomNumbersToFile(string filePath, int count, int minValue, int maxValue)
    {
        Random random = new Random();
        using (StreamWriter sw = new StreamWriter(filePath))
        {
            for (int i = 0; i < count; i++)
            {
                int num = random.Next(minValue, maxValue);
                sw.WriteLine(num);
            }
        }
    }

    static List<int> ReadDataFromFile(string filePath, Mutex mutex)
    {
        List<int> data = new List<int>();

        mutex.WaitOne();
        try
        {
            using (StreamReader sr = new StreamReader(filePath))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    if (int.TryParse(line, out int num))
                    {
                        data.Add(num);
                    }
                }
            }
        }
        catch (Exception e)
        {
            Console.WriteLine("Помилка: " + e.Message);
        }
        finally
        {
            mutex.ReleaseMutex();
        }

        return data;
    }

    static void BubbleSortWithLogging(List<int> data, string filePath, Mutex mutex)
    {
        int n = data.Count;
        bool swapped;

        for (int i = 0; i < n - 1; i++)
        {
            swapped = false;
            for (int j = 0; j < n - i - 1; j++)
            {
                if (data[j] > data[j + 1])
                {
                    int temp = data[j];
                    data[j] = data[j + 1];
                    data[j + 1] = temp;
                    swapped = true;
                }
                WriteDataToFile(data, filePath, mutex);
            }
            DisplayFileContent(filePath);
            Thread.Sleep(100);

            if (!swapped)
                break;
        }
    }

    static void WriteDataToFile(List<int> data, string filePath, Mutex mutex)
    {
        mutex.WaitOne();
        try
        {
            using (StreamWriter sw = new StreamWriter(filePath))
            {
                foreach (int num in data)
                {
                    sw.WriteLine(num);
                }
            }
        }
        catch (Exception e)
        {
            Console.WriteLine("Помилка: " + e.Message);
        }
        finally
        {
            mutex.ReleaseMutex();
        }
    }

    static void DisplayFileContent(string filePath)
    {
        try
        {
            using (StreamReader sr = new StreamReader(filePath))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    Console.Write(line + " ");
                }
                Console.WriteLine();
            }
        }
        catch (Exception e)
        {
            Console.WriteLine("Помилка: " + e.Message);
        }
    }
}
