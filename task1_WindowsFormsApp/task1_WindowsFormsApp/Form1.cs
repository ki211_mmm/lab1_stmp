﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;


namespace task1_WindowsFormsApp
{
    public partial class Form1 : Form
    {
        private int[] data;
        private const int arraySize = 20;
        private const string filePath = @"E:\labs\СТМП\lab1\data.dat";
        private Timer timer;
        public Form1()
        {
            InitializeComponent();
            LoadData();
            DisplayData();
            timer = new Timer();
            timer.Interval = 1000;
            timer.Tick += Timer_Tick;
            timer.Start();

        }
        private void Timer_Tick(object sender, EventArgs e)
        {
            if (FileChanged())
            {
                LoadData();
                DisplayData();
            }
        }
        private bool FileChanged()
        {
            DateTime currentLastWriteTime = File.GetLastWriteTime(filePath);
            if (currentLastWriteTime != lastWriteTime)
            {
                lastWriteTime = currentLastWriteTime;
                return true;
            }
            return false;
        }

        private DateTime lastWriteTime;
        private void LoadData()
        {
            try
            {
                if (File.Exists(filePath))
                {
                    string[] lines = File.ReadAllLines(filePath);
                    data = new int[arraySize];
                    for (int i = 0; i < arraySize; i++)
                    {
                        if (int.TryParse(lines[i], out int number))
                        {
                            data[i] = number;
                        }
                    }
                }
                else
                {
                    data = new int[arraySize];
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Сталася помилка при завантаженні даних: " + ex.Message, "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void DisplayData()
        {
            Controls.Clear();

            for (int i = 0; i < arraySize; i++)
            {
                Label label = new Label();
                label.Text = new string('*', data[i]);
                label.AutoSize = true;
                label.Location = new Point(20, 20 + i * 20);
                Controls.Add(label);
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            LoadData();
            DisplayData();
        }
    }
}
